# Noir Wallpapers



## Information

This repository contains my personal favorite dark bluish grey wallpapers.
In 2016, I found them in [DeviantArt.com](https://www.deviantart.com/) site, by a user named: [charlie-henson](https://charlie-henson.deviantart.com/)

But for some reason, it is a deactivated account now.

So, I thought to host them here, since they are a really good dark bluish grey wallpapers.



## Old links
- [ ] [http://charlie-henson.deviantart.com/gallery/51190820/Noir-Style?offset=0](http://charlie-henson.deviantart.com/gallery/51190820/Noir-Style?offset=0)
- [ ] [http://charlie-henson.deviantart.com/gallery/58735729/Noir-Style-HDR-Wallpapers](http://charlie-henson.deviantart.com/gallery/58735729/Noir-Style-HDR-Wallpapers)



## Important note

I'm **not** the *author* nor the *owner* of those wallpapers.
I just happen to host them here in gitlab.
